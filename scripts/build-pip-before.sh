#!/usr/bin/env bash
set -e

echo "Setting Tensorflow Build Flags"
export TF_NEED_CUDA=1
export TF_CUDA_COMPUTE_CAPABILITIES=3.7
export GCC_HOST_COMPILER_PATH=/usr/bin/gcc
export TF_NEED_VERBS=0
export TF_NEED_GCP=0
export TF_NEED_HDFS=0
export TF_NEED_JEMALLOC=1
export TF_NEED_OPENCL=0
export TF_CUDA_CLANG=0
export TF_NEED_MKL=0
export TF_ENABLE_XLA=0
export PYTHON_BIN_PATH=/usr/bin/python3
export PYTHON_LIB_PATH=/usr/lib/python3/dist-packages
export TF_CUDA_VERSION=$CUDA_VERSION
export TF_CUDNN_VERSION=$CUDNN_VERSION
export CUDA_TOOLKIT_PATH=/usr/local/cuda
export CUDNN_INSTALL_PATH=/usr/local/cuda-8.0
export CUDNN_INSTALL_PATH=/usr/lib/x86_64-linux-gnu
export LD_LIBRARY_PATH=/usr/local/cuda/extras/CUPTI/lib64:$LD_LIBRARY_PATH
export CC_OPT_FLAGS='-march="broadwell"'
echo "Checking Tensorlfow Build Flasgs"
echo "TF_NEED_CUDA=$TF_NEED_CUDA"
echo "TF_CUDA_COMPUTE_CAPABILITIES=$TF_CUDA_COMPUTE_CAPABILITIES"
echo "TF_NEED_JEMALLOC=$TF_NEED_JEMALLOC"
echo "TF_NEED_VERBS=$TF_NEED_VERBS"
echo "TF_NEED_GCP=$TF_NEED_GCP"
echo "TF_NEED_HDFS=$TF_NEED_HDFS"
echo "TF_NEED_OPENCL=$TF_NEED_OPENCL"
echo "TF_CUDA_CLANG=$TF_CUDA_CLANG"
echo "TF_NEED_MKL=$TF_NEED_MKL"
echo "TF_ENABLE_XLA=$TF_ENABLE_XLA"
echo "TF_CUDA_VERSION=$TF_CUDA_VERSION"
echo "TF_CUDNN_VERSION=$TF_CUDNN_VERSION"
echo "GCC_HOST_COMPILER_PATH=$GCC_HOST_COMPILER_PATH"
echo "PYTHON_BIN_PATH=$PYTHON_BIN_PATH"
echo "PYTHON_LIB_PATH=$PYTHON_LIB_PATH"
echo "LD_LIBRARY_PATH=$LD_LIBRARY_PATH"
echo "CUDA_TOOLKIT_PATH=$CUDA_TOOLKIT_PATH"
echo "CUDNN_INSTALL_PATH=$CUDNN_INSTALL_PATH"
echo "CC_OPT_FLAGS=$CC_OPT_FLAGS"
echo "Tensorflow r1.0 work-around for CUDNN"
mkdir -p $CUDNN_INSTALL_PATH/include
cp /usr/include/cudnn.h $CUDNN_INSTALL_PATH/include
ls $CUDNN_INSTALL_PATH/include

#echo "Cloning git"
#git clone --recurse-submodules https://github.com/tensorflow/tensorflow.git /tensorflow