#!/usr/bin/env bash
set -e
./configure
bazel build -c opt --config=cuda --verbose_failures --jobs=1 --ignore_unsupported_sandboxing //tensorflow/tools/pip_package:build_pip_package
bazel-bin/tensorflow/tools/pip_package/build_pip_package $CI_PROJECT_DIR/output
ls $CI_PROJECT_DIR/output
